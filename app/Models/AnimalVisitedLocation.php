<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnimalVisitedLocation extends Model
{
    use HasFactory;

    protected $table = 'animal_visited_locations';

    public $timestamps = false;

    protected $fillable = [
        'animalId',
        'dateTimeOfVisitLocationPoint',
        'locationPointId'
    ];
}
