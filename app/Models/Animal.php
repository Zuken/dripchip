<?php

namespace App\Models;

use DateTime;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

class Animal extends Model
{
    use HasFactory;

    protected $table = 'animals';

    const AVAILABLE_LIFE_STATUSES = ['ALIVE', 'DEAD'];
    const AVAILABLE_GENDERS = ['MALE', 'FEMALE', 'OTHER'];

    public $timestamps = false;

    protected $dates = [
        'chippingDateTime', 'deathDateTime'
    ];

    protected $fillable = [
        'weight',
        'length',
        'height',
        'gender',
        'lifeStatus',
        'chippingDateTime',
        'chipperId',
        'chippingLocationId',
        'deathDateTime'
    ];

    public function types(): HasMany
    {
        return $this->hasMany(AnimalsTypes::class, 'animalId');
    }

    public function visitedLocations(): HasMany
    {
        return $this->hasMany(AnimalVisitedLocation::class, 'locationPointId');
    }

    public function delete()
    {
        $types = $this->types()->get();
        $visitedLocations = $this->visitedLocations()->get();

        $result = parent::delete();

        if ($result === true) {
            $types->each(function (AnimalsTypes $animalType) {
                $animalType->delete();
            });
            $visitedLocations->each(function (AnimalVisitedLocation $visitedLocation) {
                $visitedLocation->delete();
            });
        }

        return $result;
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return Carbon::instance($date)->toIso8601String();
    }

    public function getItem()
    {
        $result = $this->attributesToArray();
//        $result['chippingDateTime'] = $this->chippingDateTime->format('c');
        $this->chippingDateTime;
        $result['animalTypes'] = $this
            ->types
            ->map(function (AnimalsTypes $animalType) {
                return $animalType->typeId;
            })
            ->toArray();
        $result['visitedLocations'] = $this
            ->visitedLocations
            ->map(function (AnimalVisitedLocation $visitedLocation) {
                return $visitedLocation->id;
            })
            ->toArray();

        return $result;
    }
}
