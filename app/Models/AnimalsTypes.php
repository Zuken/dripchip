<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnimalsTypes extends Model
{
    use HasFactory;

    protected $table = 'animals_types';

    public $timestamps = false;

    protected $fillable = [
        'animalId',
        'typeId'
    ];
}
