<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Account;
use Illuminate\Support\Facades\Hash;

class AuthenticateBasic
{

    public function handle(Request $request, $next)
    {
        $authorization = $request->header('authorization');
        if ($authorization) {
            list($tokenType, $token) = explode(' ', $authorization);
            $tokenType = strtolower($tokenType);
            if ($tokenType === 'basic') {
                $token = base64_decode($token);
                list($email, $password) = explode(':', $token);
                if (Auth::attempt(['email' => $email, 'password' => $password])) {
                    return $next($request);
                }
            }
            return response(status: 401);
        }
        return $next($request);
    }
}
