<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Animal;
use App\Models\AnimalsTypes;
use App\Models\AnimalType;
use App\Models\AnimalVisitedLocation;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AnimalController extends Controller
{
    public function create(Request $request)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        $validator = Validator::make($request->all(), [
            'animalTypes' => 'required|array',
            'weight' => 'required|numeric|min:1',
            'length' => 'required',
            'height' => 'required',
            'gender' => ['required', Rule::in(Animal::AVAILABLE_GENDERS)],
            'chipperId' => 'required|numeric|min:1',
            'chippingLocationId' => 'required|numeric|min:1'
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $data = [
            'weight' => $request->float('weight'),
            'length' => $request->float('length'),
            'height' => $request->float('height'),
            'gender' => $request->string('gender'),
            'chipperId' => $request->integer('chipperId'),
            'chippingLocationId' => $request->integer('chippingLocationId')
        ];

        $chipper = Account::find($data['chipperId']);

        if (!$chipper) {
            return response(status: 404);
        }

        $chippingLocation = Location::find($data['chippingLocationId']);

        if (!$chippingLocation) {
            return response(status: 404);
        }

        $types = $request->input('animalTypes', []);
        $visitedLocations = $request->input('visitedLocations', []);

        $animal = Animal::create($data);

        $result = array_merge($animal->attributesToArray(), [
            'animalTypes' => $types,
            'visitedLocations' => []
        ]);

        foreach ($types as $type) {
            AnimalsTypes::create([
                'animalId' => $animal->id,
                'typeId' => $type
            ]);
        }

        foreach ($visitedLocations as $location) {
            $visitedLocation = AnimalVisitedLocation::create([
                'animalId' => $animal->id,
                'locationPointId' => $location,
            ]);
            $result['visitedLocations'][] = $visitedLocation->id;
        }

        return response($result, 201);
    }

    public function show(int $animalId = 0)
    {
        if ($animalId <= 0) {
            return response(status: 400);
        }

        $animal = Animal::find($animalId);

        if (!$animal) {
            return response(status: 404);
        }

        return response($animal->getItem());
    }

    public function update(Request $request, int $animalId = 0)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        $validator = Validator::make($request->all(), [
            'weight' => 'required|numeric|min:1',
            'length' => 'required',
            'height' => 'required',
            'gender' => ['required', Rule::in(Animal::AVAILABLE_GENDERS)],
            'lifeStatus' => ['required', Rule::in(Animal::AVAILABLE_LIFE_STATUSES)],
            'chipperId' => 'required|numeric|min:1',
            'chippingLocationId' => 'required|numeric|min:1'
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $animal = Animal::find($animalId);

        if (!$animal) {
            return response(status: 404);
        }

        $data = [
            'weight' => $request->float('weight'),
            'length' => $request->float('length'),
            'height' => $request->float('height'),
            'gender' => $request->string('gender'),
            'lifeStatus' => $request->string('lifeStatus'),
            'chipperId' => $request->integer('chipperId'),
            'chippingLocationId' => $request->integer('chippingLocationId')
        ];

        if ($data['lifeStatus'] == 'ALIVE' && $animal->lifeStatus == 'DEAD') {
            return response(status: 400);
        }
        // Новая точка чипирования совпадает с первой посещенной точкой локации(400)

        $chipper = Account::find($data['chipperId']);

        if (!$chipper) {
            return response(status: 404);
        }

        $chippingLocation = Location::find($data['chippingLocationId']);

        if (!$chippingLocation) {
            return response(status: 404);
        }

        $animal->fill($data)->save();

        return response($animal->getItem());
    }

    public function delete(int $animalId = 0)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        if ($animalId <= 0) {
            return response(status: 400);
        }

        // Животное покинуло локацию чипирования, при этом есть другие посещенные точки(400)

        $animal = Animal::find($animalId);

        if (!$animal) {
            return response(status: 404);
        }

        $animal->delete();
    }

    public function search(Request $request)
    {
        $validator = Validator::make($request->query(), [
            'from' => 'numeric|min:0',
            'size' => 'numeric|min:1',
//            'startDateTime' => '',
//            'endDateTime' => '',
            'chipperId' => 'numeric|min:1',
            'chippingLocationId' => 'numeric|min:1',
            'lifeStatus' => [Rule::in(Animal::AVAILABLE_LIFE_STATUSES)],
            'gender' => [Rule::in(Animal::AVAILABLE_GENDERS)]
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }
        $size = (int)$request->query('size', 10);
        $from = (int)$request->query('from');
        $startDateTime = $request->query('startDateTime');
        $endDateTime = $request->query('endDateTime');
        $chipperId = (int)$request->query('chipperId');
        $chippingLocationId = (int)$request->query('chippingLocationId');
        $lifeStatus = $request->query('lifeStatus');
        $gender = $request->query('gender');

        $animals = Animal::orderBy('id');

        if ($startDateTime) {
            $animals->where('chippingDateTime', '>=', $startDateTime);
        }
        if ($endDateTime) {
            $animals->where('chippingDateTime', '<=', $endDateTime);
        }
        if ($chipperId) {
            $animals->where('chipperId', '=', $chipperId);
        }
        if ($chippingLocationId) {
            $animals->where('chippingLocationId', '=', $chippingLocationId);
        }
        if ($lifeStatus) {
            $animals->where('lifeStatus', '=', $lifeStatus);
        }
        if ($gender) {
            $animals->where('gender', '=', $gender);
        }

        $animals = $animals
            ->offset($from)
            ->limit($size)
            ->get()
            ->map(function (Animal $animal) {
                return $animal->getItem();
            })->toArray();

        return response($animals);
    }

    public function addAnimalType($animalId, $typeId)
    {
        if (!Auth::getUser()) {
            return response(status: 201);
        }
        $animalId = (int)$animalId;
        $typeId = (int)$typeId;
        if ($animalId <= 0 || $typeId <= 0) {
            return response(status: 400);
        }

        $animal = Animal::find($animalId);
        $type = AnimalType::find($typeId);

        if (!$animal || !$type) {
            return response(status: 404);
        }

        $has_type = AnimalsTypes::where('animalId', '=', $animalId)->where('typeId', '=', $typeId)->first();
        if ($has_type) {
            return response(status: 409);
        }

        AnimalsTypes::create([
            'animalId' => $animalId,
            'typeId' => $typeId
        ]);

        return response($animal->getItem(), 201);
    }

    public function updateAnimalType(Request $request, $animalId)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        if ((int) $animalId <= 0) {
            return response(status: 400);
        }

        $validator = Validator::make($request->all(), [
            'oldTypeId' => ['required', 'numeric', 'min:1'],
            'newTypeId' => ['required', 'numeric', 'min:1']
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $oldTypeId = $request->integer('oldTypeId');
        $newTypeId = $request->integer('newTypeId');

        $animal = Animal::find($animalId);
        $oldType = AnimalType::find($oldTypeId);
        $newType = AnimalType::find($newTypeId);

        if (!$animal || !$oldType || !$newType) {
            return response(status: 404);
        }

        $animalType = $animal->types->where('typeId', '=', $oldTypeId)->first();

        if (!$animalType) {
            return response(status: 404);
        }

        if ($animal->types->where('typeId', '=', $newTypeId)->first()) {
            return response(status: 409);
        }

        $animalType->delete();

        AnimalsTypes::create([
            'animalId' => $animalId,
            'typeId' => $newTypeId
        ]);

        return response($animal->getItem());
    }

    public function deleteAnimalType($animalId, $typeId)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        $animalId = (int)$animalId;
        $typeId = (int)$typeId;
        if ($animalId <= 0 || $typeId <= 0) {
            return response(status: 400);
        }

        $animal = Animal::find($animalId);
        $type = AnimalType::find($typeId);

        if (!$animal || !$type) {
            return response(status: 404);
        }

        $types = $animal->types->get();

        var_dump($types);
    }

    public function showLocations(Request $request, $animalId)
    {
        if ((int)$animalId <= 0) {
            return response(status: 400);
        }

        $validator = Validator::make($request->all(), [
            'from' => ['numeric', 'min:0'],
            'size' => ['numeric', 'min:1']
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $from = $request->integer('from');
        $size = $request->integer('size', 10);

        $animal = Animal::find($animalId);

        if (!$animal) {
            return response(status: 404);
        }

        $result = $animal
            ->visitedLocations
            ->offset($from)
            ->limit($size)
            ->get()
            ->map(function (AnimalVisitedLocation $visitedLocation) {
                return $visitedLocation->attributesToArray();
            })->toArray();

        return response($result);
    }

    public function addLocation($animalId, $pointId)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        if ((int)$animalId <= 0 || (int)$pointId <= 0) {
            return response(status: 400);
        }

        $animal = Animal::find($animalId);

        if (!$animal) {
            return response(status: 404);
        }

        if ($animal->lifeStatus == 'DEAD' || $animal->chippingLocationId == $pointId) {
            return response(status: 400);
        }

        $visitedLocations = $animal->visitedLocations;

        if ($visitedLocations->contains('typeId', '=', $pointId)) {
            return response(status: 400);
        }

        $location = Location::find($pointId);

        if (!$location) {
            return response(status: 404);
        }

        $newVisitedLocation = AnimalVisitedLocation::create([
            'animalId' => $animalId,
            'locationPointId' => $pointId,
            'dateTimeOfVisitLocationPoint' => 'CURRENT_TIMESTAMP'
        ]);

        return response([
            'id' => $newVisitedLocation->id,
            'dateTimeOfVisitLocationPoint' => $newVisitedLocation->dateTimeOfVisitLocationPoint,
            'locationPointId' => $newVisitedLocation->locationPointId
        ], 201);
    }

    public function updateLocation(Request $request, $animalId)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        $data = [
            'animalId' => (int)$animalId,
            'visitedLocationPointId' => $request->integet('visitedLocationPointId'),
            'locationPointId' => $request->integet('locationPointId')
        ];

        $validator = Validator::make($data, [
            'animalId' => ['required', 'numeric', 'min:1'],
            'visitedLocationPointId' => ['required', 'numeric', 'min:1'],
            'locationPointId' => ['required', 'numeric', 'min:1']
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $animal = Animal::find($animalId);

        if (!$animal) {
            return response(status: 404);
        }

        if ($animal->chippingLocationId == $data['locationPointId']) {
            return response(status: 400);
        }

        $visitedLocations = $animal->visitedLocations;

        if ($visitedLocations->isEmpty() || $visitedLocations->contains('typeId', '=', $data['locationPointId'])) {
            return response(status: 400);
        }

        $visitedLocation = AnimalVisitedLocation::find($data['visitedLocationPointId']);

        if (!$visitedLocation) {
            return response(status: 404);
        }

        if ($visitedLocation->locationPointId == $data['locationPointId']) {
            return response(status: 400);
        }

        $location = Location::find($data['locationPointId']);

        if (!$location) {
            return response(status: 404);
        }

        $visitedLocation->fill([
            'locationPointId' => $data['locationPointId']
        ])->save();

        return response([
            'id' => $visitedLocation->id,
            'dateTimeOfVisitLocationPoint' => $visitedLocation->dateTimeOfVisitLocationPoint,
            'locationPointId' => $visitedLocation->locationPointId
        ]);
    }

    public function deleteLocation($animalId, $pointId)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        $validator = Validator::make([
            'animalId' => $animalId,
            'pointId' => $pointId
        ],[
            'animalId' => ['required', 'numeric', 'min:1'],
            'pointId' => ['required', 'numeric', 'min:1']
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $animal = Animal::find($animalId);

        if (!$animal) {
            return response(status: 404);
        }

        $visitedLocation = AnimalVisitedLocation::find($pointId);

        if (!$visitedLocation) {
            return response(status: 404);
        }

        $visitedLocations = $animal->visitedLocations;

        if (!$visitedLocations->contains($pointId)) {
            return response(status: 404);
        }

        $visitedLocation->delete();

        $visitedLocations->each(function (AnimalVisitedLocation $point) use ($animal) {
            if ($point->id == $animal->chipperId) {
                $point->delete();
            }
        });
    }
}
