<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Account;

class AuthenticationController extends Controller
{
    public function register(Request $request)
    {
        if (Auth::getUser()) {
            return response(status: 403);
        }

        $validator = Validator::make($request->all(), [
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $firstName = $request->string('firstName')->trim();
        $lastName = $request->string('lastName')->trim();
        $email = $request->string('email')->trim();
        $password = $request->string('password')->trim();

        $account = Account::firstWhere('email', $email);

        if ($account) {
            return response(status: 409);
        }

        $account = Account::create([
            'email' => $email,
            'password' => $password,
            'firstName' => $firstName,
            'lastName' => $lastName
        ]);

        $account->save();

        return response($account->attributesToArray(), 201);
    }
}
