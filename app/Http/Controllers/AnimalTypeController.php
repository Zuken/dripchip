<?php

namespace App\Http\Controllers;

use App\Models\AnimalType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AnimalTypeController extends Controller
{
    public function create(Request $request)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        $validator = Validator::make($request->all(), [
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $data = [
            'type' => $request->string('type')
        ];

        $hasType = AnimalType::where('type', '=', $data['type'])->first();

        if ($hasType) {
            return response(status: 409);
        }

        $animalType = AnimalType::create($data);

        $animalType->save();

        return response($animalType->attributesToArray(), 201);
    }

    public function read(int $typeId = 0)
    {
        if ($typeId <= 0) {
            return response(status: 400);
        }

        $animalType = AnimalType::find($typeId);

        if (!$animalType) {
            return response(status: 404);
        }

        return response($animalType->attributesToArray());
    }

    public function update(Request $request, int $typeId = 0)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        if ($typeId <= 0) {
            return response(status: 400);
        }

        $animalType = AnimalType::find($typeId);

        if (!$animalType) {
            return response(status: 404);
        }

        $validator = Validator::make($request->all(), [
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $data = [
            'type' => $request->string('type')
        ];

        if (AnimalType::where('type', '=', $data['type'])->first()) {
            return response(status: 409);
        }

        $animalType->fill($data)->save();

        return response($animalType->attributesToArray());
    }

    public function delete(int $typeId = 0)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        if ($typeId <= 0) {
            return response(status: 400);
        }

        $animalType = AnimalType::find($typeId);

        if (!$animalType) {
            return response(status: 404);
        }

        // проверить, что нет животных с таким типом

        $animalType->delete();
    }
}
