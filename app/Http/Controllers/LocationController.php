<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Location;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LocationController extends Controller
{
    public function create(Request $request)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        $validator = Validator::make($request->all(), [
            'latitude' => ['required', 'numeric', 'min:-90', 'max:90'],
            'longitude' => ['required', 'numeric', 'min:-180', 'max:180']
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $data = [
            'latitude' => $request->float('latitude'),
            'longitude' => $request->float('longitude')
        ];

        $hasLocation = Location::where('latitude', '=', $data['latitude'])
            ->where('longitude', '=', $data['longitude'])
            ->first();

        if ($hasLocation) {
            return response(status: 409);
        }

        $location = Location::create($data);

        return response($location->attributesToArray(), 201);
    }

    public function read(int $pointId = 0)
    {
        if ($pointId <= 0) {
            return response(status: 400);
        }

        $location = Location::find($pointId);

        if (!$location) {
            return response(status: 404);
        }

        return response($location->attributesToArray());
    }

    public function update(Request $request, int $pointId = 0)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        if ($pointId <= 0) {
            return response(status: 400);
        }

        $location = Location::find($pointId);

        if (!$location) {
            return response(status: 404);
        }

        $validator = Validator::make($request->all(), [
            'latitude' => 'required|numeric|min:-90|max:90',
            'longitude' => 'required|numeric|min:-180|max:180',
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $data = [
            'latitude' => $request->input('latitude'),
            'longitude' => $request->input('longitude')
        ];

        $hasLocation = Location::where('latitude', '=', $data['latitude'])->where('longitude', '=', $data['longitude'])->first();

        if ($hasLocation) {
            return response(status: 409);
        }

        $location->fill($data);
        $location->save();

        return response($location->attributesToArray());
    }

    public function delete(int $pointId = 0)
    {
        if (!Auth::getUser()) {
            return response(status: 401);
        }

        if ($pointId <= 0) {
            return response(status: 400);
        }

        $location = Location::find($pointId);

        if (!$location) {
            return response(status: 404);
        }

        $location->delete();
    }
}
