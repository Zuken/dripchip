<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Account;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    public function read(int $accountId = 0)
    {

        $account = Account::find($accountId);

        if ($account === null) {
            return response(status: 404);
        }

        return response($account->attributesToArray());
    }

    public function update(Request $request, int $accountId = 0)
    {
        if ($accountId <= 0) {
            return response(status: 400);
        }

        $user = Auth::getUser();

        if (!$user) {
            return response(status: 401);
        }

        if ($user->id != $accountId) {
            return response(status: 403);
        }

        $account = Account::find($accountId);

        if (!$account) {
            return response(status: 403);
        }

        $validator = Validator::make($request->all(), [
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $account->fill([
            'email' => $request->string('email')->trim(),
            'password' => $request->string('password')->trim(),
            'firstName' => $request->string('firstName')->trim(),
            'lastName' => $request->string('lastName')->trim()
        ]);

        if ($account->wasChanged('email') &&  Account::firstWhere('email', $account->email)) {
            return response(status: 409);
        }

        $account->save();

        return response($account->attributesToArray());
    }

    public function delete(int $accountId = 0)
    {
        $user = Auth::getUser();

        if (!$user) {
            return response(status: 401);
        }

        if ($accountId <= 0) {
            return response(status: 400);
        }

        if ($user->id != $accountId) {
            return response(status: 403);
        }

        $account = Account::find($accountId);

        if (!$account) {
            return response(status: 403);
        }

        if ($account->animals->isNotEmpty()) {
            return response(status: 400);
        }

        $account->delete();
    }

    public function search(Request $request)
    {
        $validator = Validator::make($request->query(), [
            'size' => ['numeric', 'min:1'],
            'from' => ['numeric', 'min:0']
        ]);

        if ($validator->fails()) {
            return response(status: 400);
        }

        $size = (int)$request->query('size', 10);
        $from = (int)$request->query('from');

        $firstName = $request->query('firstName');
        $lastName = $request->query('lastName');
        $email = $request->query('email');

        $accounts = Account::orderBy('id');

        if ($firstName) {
            $accounts->where('firstName', 'LIKE', "%$firstName%");
        }

        if ($lastName) {
            $accounts->where('lastName', 'LIKE', "%$lastName%");
        }

        if ($email) {
            $accounts->where('email', 'LIKE', "%$email%");
        }

        $accounts = $accounts
            ->offset($from)
            ->limit($size)
            ->get()
            ->map(function (Account $account) {
                return $account->attributesToArray();
            })->toArray();

        return response($accounts);
    }
}
