<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\AnimalController;
use App\Http\Controllers\AnimalTypeController;
use App\Http\Controllers\LocationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/registration', [AuthenticationController::class, 'register']);

/* Accounts routes */

Route::controller(AccountController::class)->group(function () {
    Route::get('/accounts/search', 'search');
    Route::get('/accounts/{accountId?}', 'read');
    Route::put('/accounts/{accountId?}', 'update');
    Route::delete('/accounts/{accountId?}', 'delete');
})->middleware('auth.basic');

Route::controller(AnimalTypeController::class)->group(function () {
    Route::get('/animals/types/{typeId?}', 'read');
    Route::post('/animals/types', 'create');
    Route::put('/animals/types/{typeId?}', 'update');
    Route::delete('/animals/types/{typeId?}', 'delete');
});

/* Animal routes */

Route::controller(AnimalController::class)->group(function () {
    Route::get('/animals/search', 'search');
    Route::post('/animals/{animalId?}', 'create');
    Route::get('/animals/{animalId?}', 'show');
    Route::put('/animals/{animalId?}', 'update');
    Route::delete('/animals/{animalId?}', 'delete');

    Route::post('/animals/{animalId}/types/{typeId}', 'addAnimalType');
    Route::put('/animals/{animalId}/types', 'updateAnimalType');
    Route::delete('/animals/{animalId}/types/{typeId}', 'deleteAnimalType');

    Route::post('/animals/{animalId}/locations/{pointId}', 'addLocation');
    Route::get('/animals/{animalId}/locations', 'showLocations');
    Route::put('/animals/{animalId}/locations', 'updateLocation');
    Route::delete('/animals/{animalId}/locations/{pointId}', 'deleteLocation');
});

Route::controller(LocationController::class)->group(function () {
    Route::get('/locations/{pointId?}', 'read');
    Route::post('/locations', 'create');
    Route::put('/locations/{pointId?}', 'update');
    Route::delete('/locations/{pointId?}', 'delete');
});
