<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('animals_types')) {
            Schema::create('animals_types', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('animalId');
                $table->unsignedBigInteger('typeId');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('animals_types');
    }
};
