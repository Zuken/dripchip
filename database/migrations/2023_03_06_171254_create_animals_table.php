<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('animals')) {
            Schema::create('animals', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->float('weight');
                $table->float('length');
                $table->float('height');
                $table->enum('gender', ['MALE', 'FEMALE', 'OTHER']);
                $table->enum('lifeStatus', ['ALIVE', 'DEAD'])->default('ALIVE');
                $table->dateTime('chippingDateTime')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->integer('chipperId');
                $table->unsignedBigInteger('chippingLocationId');
                $table->dateTime('deathDateTime')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('animals');
    }
};
